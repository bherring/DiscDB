module DiscDBExceptions
  module IO
    class UnsupportedFileFormat < Exception
      def initialize(message)
        super
        @message = message
      end
    end
  end
end