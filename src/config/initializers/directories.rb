EXPORTS_DIRECTORY = File.join(Rails.public_path, 'exports')

Dir.mkdir(EXPORTS_DIRECTORY) unless File.directory?(EXPORTS_DIRECTORY)