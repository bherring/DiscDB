module TvShowsHelper
  def valid_import_formats
    (TvShow::IMPORT_FORMATS.map { |f| f[:extentions] }).flatten
  end

  def valid_import_formats_mime
    (TvShow::IMPORT_FORMATS.map { |f| f[:mime_type] }).flatten
  end
end
