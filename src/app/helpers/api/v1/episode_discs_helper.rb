module API::V1::EpisodeDiscsHelper
  def combined_episodes(episode_disc)
    EpisodeDisc.where(disc_id: episode_disc.disc.id, disc_order: episode_disc.disc_order).map { |ed| ed.episode } if episode_disc.combined_episodes?
  end
end
