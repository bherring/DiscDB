module ApplicationHelper
  def tvdb_url(resource)
    case resource
      when TvShow
        query = "tab=series&id=#{resource.tvdb_id}"
      when Season
        query = "tab=season&seriesid=#{resource.tv_show.tvdb_id}&seasonid=#{resource.tvdb_id}"
      else
        query = nil
    end

    "http://thetvdb.com/?#{query}"
  end

  def current_user_can_access_api
    current_user.has_role?(:api_read_write) || current_user.has_role?(:api_read_only)
  end

  def boolean_to_string(boolean)
    if boolean.is_a?(String)
      case boolean.downcase
        when 'true', '1'
          'Yes'
        when 'false', '0'
          'No'
        else
          nil
      end
    else
      case boolean
        when true, 1
          'Yes'
        when false, 0
          'No'
        else
          nil
      end
    end
  end
end
