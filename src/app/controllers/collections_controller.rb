class CollectionsController < ApplicationController
  before_action :set_collection, only: [:show, :edit, :update, :destroy, :associate_disc, :perform_associate_disc]
  before_action :set_tv_show, only: [:index, :new, :create, :destroy, :associate_disc, :perform_associate_disc]

  # GET /collections
  def index
    @collections = @tv_show.collections
  end

  # GET /collections/1
  def show
  end

  # GET /collections/new
  def new
    @collection = @tv_show.collections.new
  end

  # GET /collections/1/edit
  def edit
  end

  # POST /collections
  def create
    @collection = @tv_show.collections.new(collection_params)

    @collection.user = current_user

    respond_to do |format|
      if @collection.save
        format.html { redirect_to [@collection.tv_show, @collection], notice: 'Collection was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /collections/1
  def update
    respond_to do |format|
      if @collection.update(collection_params)
        format.html { redirect_to [@collection.tv_show, @collection], notice: 'Collection was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /collections/1
  def destroy
    @collection.destroy

    respond_to do |format|
      format.html { redirect_to tv_show_path(@tv_show), notice: 'Collection was successfully destroyed.' }
    end
  end

  # GET /collections/1/associate_disc
  def associate_disc
  end

  # GET /collections/1/perform_associate_disc
  def perform_associate_disc
    @collection.discs = Disc.find(params[:discs])

    respond_to do |format|
      if @collection.save
        format.html { redirect_to [@collection.tv_show, @collection], notice: 'Discs were successfully associated.' }
      else
        format.html { render :associate_disc }
      end

    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collection
      @collection = Collection.find_by_tv_show_id_and_name(TvShow.friendly.find(params[:tv_show_id]), params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def collection_params
      params.require(:collection).permit(:disc_collection_id, :name, :description, :region_code, :region_name)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_tv_show
      @tv_show = TvShow.friendly.find(params[:tv_show_id])
    end
end
