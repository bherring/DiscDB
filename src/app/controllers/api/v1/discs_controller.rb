class API::V1::DiscsController < API::APIBaseController
  load_and_authorize_resource
  before_action :set_disc, only: [:show, :update, :destroy]
  before_action :set_season, only: [:create]

  # GET /discs.json
  def index
    if params[:season_id]
      @discs = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:season_id]).discs
    elsif params[:collection_id]
      @discs = Collection.find_by_tv_show_id_and_name(TvShow.friendly.find(params[:tv_show_id]), TvShow.friendly.find(params[:collection_id])).discs
    else
      @discs = Disc.filter(params.slice(:disc_label))
      @include_all_episodes = params[:include_all_episodes] ? (params[:include_all_episodes].downcase == 'true' ? true : false) : false
    end
  end

  # GET /discs/1.json
  def show
  end

  # POST /discs.json
  def create
    @disc = @season.discs.new(disc_params)
    @disc.tv_show = @season.tv_show

    respond_to do |format|
      if @disc.save
        format.json { render :show, status: :created, location: tv_show_season_disc_path(@disc.tv_show, @disc.season, @disc) }
      else
        format.json { render json: @disc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /discs/1.json
  def update
    respond_to do |format|
      if @disc.update(disc_params)
        format.json { render :show, status: :ok, location: tv_show_season_disc_path(@disc.tv_show, @disc.season, @disc) }
      else
        format.json { render json: @disc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /discs/1.json
  def destroy
    @disc.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  # POST /discs/import
  def import
    uploaded_io = params[:file]
    save_path = Rails.root.join('tmp', "#{SecureRandom.hex}_#{uploaded_io.original_filename}")

    File.open(save_path, 'wb') do |file|
      file.write(uploaded_io.read)
    end

    begin
      yaml = YAML.load_file(save_path.to_s)

      Disc.import(yaml)


      respond_to do |format|
        format.json { render json: { message: 'Discs were successfully imported.', status: 200 }.to_json }
      end
    rescue Exception => e
      respond_to do |format|
        format.json { render json: { message: "Discs failed to be imported with error #{e.message}.", status: 500 }.to_json }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_disc
    @disc = Disc.find(params[:id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_season
    @season = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def disc_params
    params.require(:disc).permit(:disc_number, :disc_type, :disc_label).merge(:audit_comment => "api_key_id=#{current_api_key.id}")
  end

end
