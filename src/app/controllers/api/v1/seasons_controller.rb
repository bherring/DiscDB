class API::V1::SeasonsController < API::APIBaseController
  #load_and_authorize_resource
  before_action :set_season, only: [:show, :update, :destroy]
  before_action :set_tv_show, only: [:create]

  # GET /seasons
  def index
    if params[:tv_show_id]
      @seasons = TvShow.friendly.find(params[:tv_show_id]).seasons
    else
      @seasons = Season.filter(params.slice(:tv_show))
    end
  end

  # GET /seasons/1
  def show
  end

  # POST /seasons.json
  def create
    @season = @tv_show.seasons.new(season_params)
    authorize! :create, @season

    respond_to do |format|
      if @season.save
        format.json { render :show, status: :created, location: tv_show_season_path(@season.tv_show, @season) }
      else
        format.json { render json: @season.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /seasons/1.json
  def update
    authorize! :update, @season

    respond_to do |format|
      if @season.update(season_params)
        format.json { render :show, status: :ok, location: tv_show_season_path(@season.tv_show, @season) }
      else
        format.json { render json: @season.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seasons/1.json
  def destroy
    authorize! :destroy, @season
    @season.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_season
    @season = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def season_params
    params.require(:season).permit(:number, :tvdb_id).merge(:audit_comment => "api_key_id=#{current_api_key.id}")
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_tv_show
    @tv_show = TvShow.friendly.find(params[:tv_show_id])
  end
end
