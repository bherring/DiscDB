class API::V1::EpisodeDiscsController < API::APIBaseController
  load_and_authorize_resource
  before_action :set_episode_disc, only: [:show, :update, :destroy]
  before_action :set_season, only: [:create, :update]
  before_action :combined_episodes, only: [:show]

  # GET /episode_discs.json
  def index
    if params[:disc_id]
      @episode_discs = EpisodeDisc.where(disc_id: params[:disc_id]).all
    elsif params[:episode_id]
      @episode_discs = EpisodeDisc.where(episode_id: params[:episode_id]).all
    else
      @episode_discs = []
    end
  end

  # GET /episode_discs/1.json
  def show
  end

  # POST /episode_discs.json
  def create
    @episode_disc = EpisodeDisc.new(episode_disc_params.merge(:episode_id => params[:episode_id]))

    respond_to do |format|
      if @episode_disc.save
        format.json { render :show, status: :created, location: api_tv_show_season_episode_episode_disc_path(@episode_disc.episode.tv_show, @episode_disc.episode.season, @episode_disc.episode, @episode_disc) }
      else
        format.json { render json: @episode_disc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /episode_discs/1.json
  def update
    respond_to do |format|
      if @episode_disc.update(episode_disc_params)
        format.json { render :show, status: :ok, location: api_tv_show_season_episode_episode_disc_path(@episode_disc.episode.tv_show, @episode_disc.episode.season, @episode_disc.episode, @episode_disc) }
      else
        format.json { render json: @episode_disc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /episode_discs/1.json
  def destroy
    @episode_disc.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_episode_disc
    @episode_disc = EpisodeDisc.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def episode_disc_params
    params.require(:episode_disc).permit(:episode_id, :disc_id, :disc_order).merge(:audit_comment => "api_key_id=#{current_api_key.id}")
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_season
    @season = Season.find(params[:season_id])
  end

  def combined_episodes
    @combined_episodes = EpisodeDisc.where(disc_id: @episode_disc.disc.id, disc_order: @episode_disc.disc_order).map { |ed| ed.episode } if @episode_disc.combined_episodes?
  end
end
