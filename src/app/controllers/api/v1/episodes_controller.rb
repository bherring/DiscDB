class API::V1::EpisodesController < API::APIBaseController
  load_and_authorize_resource
  before_action :set_episode, only: [:show, :update, :destroy]
  before_action :set_season, only: [:create]

  # GET /episodes.json
  def index
    if params[:disc_id]
      @episodes = Disc.find(params[:disc_id]).episodes
    elsif params[:season_id]
      @episodes = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:season_id]).episodes
    elsif params[:tv_show_id]
      @episodes = TvShow.friendly.find(params[:tv_show_id]).episodes
    else
      @episodes = Episode.filter(params.slice(:starts_with, :tv_show))
    end
  end

  # GET /episodes/1.json
  def show
  end

  # POST /episodes.json
  def create
    @episode = @season.episodes.new(episode_params)
    @episode.tv_show = @season.tv_show

    respond_to do |format|
      if @episode.save
        format.json { render :show, status: :created, location: tv_show_season_episodes_path(@episode.tv_show, @episode.season, @episode) }
      else
        format.json { render json: @episode.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /episodes/1.json
  def update
    respond_to do |format|
      if @episode.update(episode_params)
        format.json { render :show, status: :ok, location: tv_show_season_episodes_path(@episode.tv_show, @episode.season, @episode) }
      else
        format.json { render json: @episode.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /episodes/1.json
  def destroy
    @episode.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_episode
    @episode = Episode.find(params[:id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_season
    @season = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def episode_params
    params.require(:episode).permit(:number, :name).merge(:audit_comment => "api_key_id=#{current_api_key.id}")
  end
end
