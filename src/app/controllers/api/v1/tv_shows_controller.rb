class API::V1::TvShowsController < API::APIBaseController
  load_and_authorize_resource :find_by => :slug
  before_action :set_tv_show, only: [:show, :update, :destroy]
  before_action :set_user, only: [:create]

  # GET /tv_shows
  def index
    @tv_shows = TvShow.filter(params.slice(:starts_with))
  end

  # GET /tv_shows/1
  def show
  end

  # POST /tv_shows.json
  def create
    @tv_show = @user.tv_shows.new(tv_show_params)

    respond_to do |format|
      if @tv_show.save
        format.json { render :show, status: :created, location: @tv_show }
      else
        format.json { render json: @tv_show.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tv_shows/1.json
  def update
    respond_to do |format|
      if @tv_show.update(tv_show_params)
        format.json { render :show, status: :ok, location: @tv_show }
      else
        format.json { render json: @tv_show.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tv_shows/1.json
  def destroy
    @tv_show.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_tv_show
    @tv_show = TvShow.friendly.find(params[:id])
  end

  def set_user
    @user = current_user
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def tv_show_params
    params.require(:tv_show).permit(:name, :tvdb_id, :language).merge(:audit_comment => "api_key_id=#{current_api_key.id}")
  end
end
