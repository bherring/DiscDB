class SeasonsController < ApplicationController
  #load_and_authorize_resource
  before_action :set_season, only: [:show, :edit, :update, :destroy]
  before_action :set_tv_show, only: [:index, :new, :create]

  # GET /seasons
  def index
    @seasons = @tv_show.seasons
  end

  # GET /seasons/1
  def show
  end

  # GET /seasons/new
  def new
    @season = @tv_show.seasons.new
    authorize! :new, @season
  end

  # GET /seasons/1/edit
  def edit
    authorize! :edit, @season
  end

  # POST /seasons
  def create
    @season = @tv_show.seasons.new(season_params)
    authorize! :create, @season

    respond_to do |format|
      if @season.save
        format.html { redirect_to [@season.tv_show, @season], notice: 'Season was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /seasons/1
  def update
    authorize! :update, @season

    respond_to do |format|
      if @season.update(season_params)
        format.html { redirect_to [@season.tv_show, @season], notice: 'Season was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /seasons/1
  def destroy
    authorize! :destroy, @season
    @season.destroy

    respond_to do |format|
      format.html { redirect_to tv_show_seasons_path, notice: 'Season was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_season
      @season = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def season_params
      params.require(:season).permit(:number, :tvdb_id)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_tv_show
      @tv_show = TvShow.friendly.find(params[:tv_show_id])
    end
end
