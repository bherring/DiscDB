class EpisodesController < ApplicationController
  load_and_authorize_resource
  before_action :set_episode, only: [:show, :edit, :update, :destroy]
  before_action :set_season, only: [:new, :create]

  # GET /episodes
  def index
    if params[:disc_id]
      @episodes = Disc.find(params[:disc_id]).episodes
    elsif params[:season_id]
      @episodes = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:season_id]).episodes
    elsif params[:tv_show_id]
      @episodes = TvShow.friendly.find(params[:tv_show_id]).episodes
    end
  end

  # GET /episodes/1
  def show
  end

  # GET /episodes/new
  def new
    @episode = @season.episodes.new
    @episode.tv_show = @season.tv_show
    @episode.disc = Disc.find_by_id(params[:disc_id]) if params[:disc_id]
  end

  # GET /episodes/1/edit
  def edit
  end

  # POST /episodes
  def create
    @episode = @season.episodes.new(episode_params)
    @episode.tv_show = @season.tv_show

    respond_to do |format|
      if @episode.save
        format.html { redirect_to [@episode.tv_show, @episode.season, @episode], notice: 'Episode was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /episodes/1
  def update
    respond_to do |format|
      if @episode.update(episode_params)
        format.html { redirect_to [@episode.tv_show, @episode.season, @episode], notice: 'Episode was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /episodes/1
  def destroy
    @episode.destroy
    respond_to do |format|
      format.html { redirect_to tv_show_season_episodes_path, notice: 'Episode was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_episode
      @episode = Episode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def episode_params
      params.require(:episode).permit(:number, :name)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_season
      @season = Season.find(params[:season_id])
    end
end
