class TvShowsController < ApplicationController
  load_and_authorize_resource :find_by => :slug
  before_action :set_tv_show, only: [:show, :edit, :update, :destroy, :export]

  # GET /tv_shows
  def index
    @tv_shows = TvShow.all
  end

  # GET /tv_shows/1
  def show
  end

  # GET /tv_shows/new
  def new
    @tv_show = TvShow.new
  end

  # GET /tv_shows/1/edit
  def edit
  end

  # POST /tv_shows
  def create
    @tv_show = TvShow.new(tv_show_params)

    respond_to do |format|
      if @tv_show.save
        format.html { redirect_to @tv_show, notice: 'Tv show was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /tv_shows/1
  def update
    respond_to do |format|
      if @tv_show.update(tv_show_params)
        format.html { redirect_to @tv_show, notice: 'Tv show was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /tv_shows/1
  def destroy
    @tv_show.destroy
    respond_to do |format|
      format.html { redirect_to tv_shows_url, notice: 'Tv show was successfully destroyed.' }
    end
  end

  # GET /tv_shows/1/export
  def export
    authorize! :export, current_user

    respond_to do |format|
      format.yaml {
        file_name = @tv_show.to_yaml_file

        response.headers['Content-Length'] = File.stat(file_name).size.to_s
        send_file file_name, type: 'application/x-yaml'
      }
    end
  end

  # GET /tv_shows/import
  def import
  end

  # POST /tv_shows/perform_import
  def perform_import
    uploaded_io = params[:file]
    save_path = Rails.root.join('tmp', "#{SecureRandom.hex}_#{uploaded_io.original_filename}")

    File.open(save_path, 'wb') do |file|
      file.write(uploaded_io.read)
    end

    begin
      TvShow.import_from_file(save_path, current_user)

      respond_to do |format|
        format.html { redirect_to root_path, notice: 'Discs were successfully imported.' }
      end
    rescue Exception => e
      respond_to do |format|
        format.html { redirect_to root_path, alert: "Discs failed to be imported with error #{perform_import_error_message(e)}." }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tv_show
      @tv_show = TvShow.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tv_show_params
      params.require(:tv_show).permit(:name, :tvdb_id, :language, :user_id)
    end

    def perform_import_error_message(exception)
      case exception
        when ActiveRecord::RecordInvalid
          "#{exception.message}: #{exception.record}"
        else
          exception.message
      end
    end
end
