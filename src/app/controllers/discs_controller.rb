class DiscsController < ApplicationController
  load_and_authorize_resource
  before_action :set_disc, only: [:show, :edit, :update, :destroy]
  before_action :set_season, only: [:new, :create, :index]

  # GET /discs
  def index
    if params[:season_id]
      @discs = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:season_id]).discs
    elsif params[:collection_id]
      @discs = Collection.find_by_tv_show_id_and_name(TvShow.friendly.find(params[:tv_show_id]), params[:collection_id]).discs
    else
      @discs = Disc.filter(params.slice(:disc_label))
      @include_all_episodes = params[:include_all_episodes] ? (params[:include_all_episodes].downcase == 'true' ? true : false) : false
    end
  end

  # GET /discs/1
  def show
  end

  # GET /discs/new
  def new
    @disc = @season.discs.new
    @disc.tv_show = @season.tv_show
  end

  # GET /discs/1/edit
  def edit
  end

  # POST /discs
  def create
    @disc = @season.discs.new(disc_params)
    @disc.tv_show = @season.tv_show

    respond_to do |format|
      if @disc.save
        format.html { redirect_to [@disc.tv_show, @disc.season, @disc], notice: 'Disc was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /discs/1
  def update
    respond_to do |format|
      if @disc.update(disc_params)
        format.html { redirect_to [@disc.tv_show, @disc.season, @disc], notice: 'Disc was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /discs/1
  def destroy
    @disc.destroy
    respond_to do |format|
      format.html { redirect_to tv_show_season_discs_path, notice: 'Disc was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_disc
      @disc = Disc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def disc_params
      params.require(:disc).permit(:disc_number, :disc_type, :disc_label)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_season
      @season = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:season_id])
    end
end
