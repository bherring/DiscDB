json.extract! episode, :id, :name, :number, :created_at, :updated_at
json.tv_show episode.tv_show.name
json.season episode.season.number