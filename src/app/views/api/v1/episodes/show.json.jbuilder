json.partial! "api/#{self.controller_path.split('/').second}/episodes/episode", episode: @episode
json.url api_tv_show_season_episode_url(@episode.tv_show, @episode.season, @episode, format: :json)