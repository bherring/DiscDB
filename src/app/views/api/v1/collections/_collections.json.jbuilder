json.array!(collections) do |collection|
  json.partial! "api/#{self.controller_path.split('/').second}/collections/collection", collection: collection
  json.url api_tv_show_collection_url(collection.tv_show, collection, format: :json)
end