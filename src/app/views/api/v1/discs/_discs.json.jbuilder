json.array!(discs) do |disc|
    json.partial! "api/#{self.controller_path.split('/').second}/discs/disc", disc: disc
    json.url api_tv_show_season_disc_url(disc.tv_show, disc.season, disc, format: :json)

  json.episode_discs do
    json.partial! "api/#{self.controller_path.split('/').second}/episode_discs/episode_discs", episode_discs: disc.episode_discs
  end
end