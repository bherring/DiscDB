json.extract! disc, :id, :disc_number, :disc_type, :disc_label, :season_id, :tv_show_id, :created_at, :updated_at
json.tv_show disc.tv_show.name
json.season disc.season.number