class APIAbility
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    base_models = [TvShow, Season, Disc, Episode, EpisodeDisc]

    if user.has_role?(:api_read_write)
      can :manage, base_models
    elsif user.has_role?(:api_read_only)
      can :read, base_models
    end
  end
end