class Season < ActiveRecord::Base
  include Filterable

  audited

  belongs_to :user
  belongs_to :tv_show

  has_many :episodes, dependent: :destroy
  has_many :discs, dependent: :destroy


  validates :number, presence: true
  validates :tv_show_id, presence: true

  validates_uniqueness_of :number, scope: :tv_show_id


  scope :tv_show, -> (tv_show) { where('tv_show_id = ?', TvShow.find_by_name(tv_show).id) }


  def to_s
    self.number.to_s
  end

  def to_hash
    {
        number: self.number,
        tvdb_id: self.tvdb_id,
        discs: self.discs.map {|disc| disc.to_hash}
    }
  end

  def to_param
    number.to_s
  end
end
