class CreateTvShows < ActiveRecord::Migration[4.2]
  def change
    create_table :tv_shows do |t|
      t.string :name
      t.string :tvdb_id
      t.string :language
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
