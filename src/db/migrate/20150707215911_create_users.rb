class CreateUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :users do |t|
      t.string :fname
      t.string :lname
      t.string :time_zone
      t.integer :roles_mask
      t.string :provider
      t.integer :uid

      t.timestamps null: false
    end
  end
end
