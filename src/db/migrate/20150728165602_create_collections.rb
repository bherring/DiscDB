class CreateCollections < ActiveRecord::Migration[4.2]
  def change
    create_table :collections do |t|
      t.string :name
      t.string :description
      t.string :region_code
      t.string :region_name
      t.string :slug
      t.integer :tv_show_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
