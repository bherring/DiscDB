class CreateEpisodeDiscs < ActiveRecord::Migration[4.2]
  def change
    create_table :episode_discs do |t|
      t.integer :episode_id
      t.integer :disc_id
      t.integer :disc_order

      t.timestamps null: false
    end

    add_index :episode_discs, [:episode_id, :disc_id], :unique => true
  end
end
