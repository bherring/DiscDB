# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



#
# System User
#

systemUser = AppConfig.get(%w(system_user), APP_CONFIG)

# Email
if ENV["SYSTEM_USER_EMAIL_PATH"]
  adminUserEmail = File.read(ENV["SYSTEM_USER_EMAIL_PATH"])
elsif ENV["SYSTEM_USER_EMAIL"]
  adminUserEmail = ENV["SYSTEM_USER_EMAIL"]
else
  skipAdminUserCreation = true
end

if (User.where(fname: systemUser['fname'], lname: systemUser['lname']).first == nil)
  puts "seeds.rb -> Creating System User..."
  user                          = User.new
  user.email                    = systemUser['email']
  user.fname                    = systemUser['fname']
  user.lname                    = systemUser['lname']
  user.skip_password_validation = true
  user.save!
end


#
# Local admin
#

adminUserFname        = ENV['ADMIN_USER_FIRST_NAME'] || 'admin'
adminUserLName        = ENV['ADMIN_USER_LAST_NAME'] || 'user'
adminUserEmail        = nil
adminUserPassword     = nil
adminUserRoles        = [:admin]
skipAdminUserCreation = false

# Email
if ENV["ADMIN_USER_EMAIL_PATH"]
  adminUserEmail = File.read(ENV["ADMIN_USER_EMAIL_PATH"])
elsif ENV["ADMIN_USER_EMAIL"]
  adminUserEmail = ENV["ADMIN_USER_EMAIL"]
else
  skipAdminUserCreation = true
end

# Password
if ENV["ADMIN_USER_PASSWORD_PATH"]
  adminUserPassword = File.read(ENV["ADMIN_USER_PASSWORD_PATH"])
elsif ENV["ADMIN_USER_PASSWORD"]
  adminUserPassword = ENV["ADMIN_USER_PASSWORD"]
else
  skipAdminUserCreation = true
end

if (skipAdminUserCreation == false && User.where(email: 'brad@bherville.com').first == nil)
  puts "seeds.rb -> Creating Admin User..."
  user                        = User.new
  user.email                  = adminUserEmail
  user.password               = adminUserPassword
  user.password_confirmation  = adminUserPassword
  user.roles << adminUserRoles
  user.confirm
  user.save!
end